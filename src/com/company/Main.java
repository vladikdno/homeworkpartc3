package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        3. Ввести n строк с консоли. Вывести на консоль те строки, которые содержат ключевое слово (ключевое слово задано изначально, например “tree”).
//        3.1* Вывести на консоль те строки, которые содержат одно из заданных ключевых слов (ключевые слова заданы изначально, в массиве например “tree”, “meat”).


        Scanner newSc = new Scanner(System.in); //Вызываем масив с названием newSc

        System.out.println("Enter number of rows: ");
        int numRows = Integer.parseInt(newSc.nextLine());   //Считали количество строк с консоли, которые ввел user с помощью nextLine()
        String[] rowsArray = new String[numRows];   //Определились с колиством индексов в массиве, с помощью int numRows
        String keyword = "tree";

        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Enter the row number %d: ", i + 1)); //консоль говорит нам введи что-то начиная с первой строки
            String row = newSc.nextLine();   //сканер считывает то что ты вбил в консоли
            rowsArray[i] = row;     //теперь массиву rowsArray присваиваеться то, что мы написали выше
        }

//        for (String row : rowsArray) {        //ищем один кейверд,
//            if (row.contains(keyword)) {
//                System.out.println(String.format("keyword on this row: %s", row));
//            }
//        }

        String[] suppressions = new String[]{"tree", "meat"};       //ищем 2 кейверда и поэтому создаем цикл в цикле
        for (String row : rowsArray) {
            for (String suppresion : suppressions) {
                if (row.contains(suppresion)) {     // здесь я уже смотрю есть ли у меня в данной строке suppresion которую мы проверяем сейчас
                    System.out.println(String.format("keyword on this row: %s", row));
                }
            }
        }


    }
}
